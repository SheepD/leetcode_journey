var canConstruct = function(ransomNote, magazine) {
  const hashify = function (str) {
      const map = {}
      for (let i = 0; i < str.length; i++) {
          map[str[i]] = map[str[i]] ? map[str[i]] + 1 : 1
      }

      return map
  }
  const rn = hashify(ransomNote)
  const mg = hashify(magazine)

  for (const char in rn) {
      if (!mg[char] || mg[char] < rn[char]) {
          return false
      }
  }

  return true
}

console.log(canConstruct("fffbfg", "effjfggbffjdgbjjhhdegh"))
console.log(canConstruct("a", "b"))
