var solution = function(isBadVersion) {
  /**
   * @param {integer} n Total versions
   * @return {integer} The first bad version
   */
  return function(n) {
    var memoBad = [];
    var leftLimit = 1;
    var rightLimit = n;

    while (true) {
      let mid = (leftLimit + rightLimit) / 2 | 0;
      console.log(`m:${mid} l:${leftLimit} r:${rightLimit}`);

      if (isBadVersion(mid)) {
        memoBad[mid] = true;

        if (memoBad[mid - 1] === false || !isBadVersion(mid - 1)) {
          return mid;
        } else {
          memoBad[mid - 1] = true;
        }
        rightLimit = mid;
      } else {
        leftLimit = leftLimit === mid ? mid + 1 : mid;
        memoBad[mid] = false;
      }
    }
  };
};

console.log(solution(function (n) { return n === 2; })(2));
