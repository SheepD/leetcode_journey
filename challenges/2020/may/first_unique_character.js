/**
 * @param {string} s
 * @return {number}
 */
var firstUniqChar = function(s) {
  const q = {}
  const tracker  = {}
  for(let i = 0; i < s.length; i++) {
      const char = s[i]
      if (!tracker[char]) {
          q[char] = i
          tracker[char] = 1
      } else {
          tracker[char]++
      }
  }

  for (char in q) {
      if (tracker[char] == 1) { return q[char] }
  }

  return -1
};

console.log(`${firstUniqChar("leetcode")} is 0`)
console.log(`${firstUniqChar("loveleetcode")} is 2`)
console.log(`${firstUniqChar("")} is -1`)