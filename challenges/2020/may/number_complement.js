var findComplement = function(num) {
  const temp = (num >>> 0).toString(2).length
  let mask = 0

  for (let i = 0; i < temp; i++) {
    mask = mask << 1 | 1
  }
  return num ^ mask
};

console.log(`${findComplement(5)} is the complement of 5`)
console.log(`${findComplement(1)} is the complement of 1`)
