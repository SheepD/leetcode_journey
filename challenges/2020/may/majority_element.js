/**
 * @param {number[]} nums
 * @return {number}
 */
var majorityElement = function(nums) {
  const counter = {}
  let max

  for (let i = 0; i < nums.length; i++) {
    counter[nums[i]] = counter[nums[i]] + 1 || 1
  }

  for (num in counter) {
    if (counter[num] > counter[max] || !max) {
        max = num
    }
  }

  return max
}

console.log(`${majorityElement([3, 2, 3])} should be 3`)
console.log(`${majorityElement([2,2,1,1,1,2,2])} should be 2`)
