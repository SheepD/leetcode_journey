var checkStraightLine = function(coordinates) {
  if (coordinates.length == 2) return true

  const calcSlope = (c1, c2) => (c2[1] - c1[1]) / (c2[0] - c1[0])
  let m = calcSlope(coordinates[0], coordinates[1])

  for (let i = 2; i < coordinates.length; i++) {
    if (calcSlope(coordinates[i], coordinates[i - 1]) != m) return false
  }

  return true
}

console.log(`${checkStraightLine([[1,2],[2,3],[3,4],[4,5],[5,6],[6,7]])} is true`)
console.log(`${checkStraightLine([[1,1],[2,2],[3,4],[4,5],[5,6],[7,7]])} is false`)
console.log(`${checkStraightLine([[-3,-2],[-1,-2],[2,-2],[-2,-2],[0,-2]])} is true`)
